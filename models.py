from peewee import *
import os

BASE_DIR = os.path.abspath(os.path.dirname(__file__))
db_name = os.path.join(BASE_DIR, 'rest.db')
db = SqliteDatabase(db_name)


class User(Model):
    name = CharField()
    age = IntegerField()

    class Meta:
        database = db


if __name__ == '__main__':
    db.create_tables([User])
    # 增加
    # for i in range(1000):
    #     User.create(name=f'用户{i}', age=int(i / 1000 * 100))

    # 查询
    for u in User.select():
        print(u.name, u.age)
    # User.delete().execute()
