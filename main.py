import functools
import time

from core.routers import Resource, FastAPI


def time_cost(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        st = time.time()
        res = func(*args, **kwargs)
        print(f"{func.__name__} cost {round(time.time() - st, 3)}s")
        return res

    return wrapper


class Temp(Resource):
    def post(self):
        result = {'code': 200, 'data': [], 'msg': 'post ok'}
        return result

    @time_cost
    def put(self):
        result = {'code': 200, 'data': [], 'msg': 'put ok'}
        return result


class Temp1(Resource):
    def get(self, id: int):
        result = {'code': 200, 'data': [{"id": id}], 'msg': 'get ok'}
        return result


temp = Temp(path="/user", tags=["用户管理"], summary="用户")
temp1 = Temp1(tags=["用户管理"], summary="用户", resource_name_dict={"get": "查询所有用户"})
app = FastAPI(
    # API 文档信息
    title="测试API",
    description="测试fastapi_restful",
    version="1.0.0",
    openapi_url="/api/v1/openapi.json",
    # 配置文档路径
    docs_url="/docs",
    redoc_url="/redoc",
)
app.add_resource(temp)
app.add_resource(temp1, path="/user/{id}")
