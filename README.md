# fastapi中类似flask_restful的框架

## 基本用法
```python
from fastapi import FastAPI
from core.routers import set_model_router
from models import User
from pydantic import BaseModel

app = FastAPI()


class UserItem(BaseModel):
    name: str
    age: int


# 设置路由
set_model_router(app, User, UserItem, "user", "用户")
```

## 参数说明
```text
"""
设置路由， 会自动生成五个路由： 获取所有， 增加一个， 获取一个， 修改一个， 删除一个
:param app: fastapi的实例对象
:param model: 要设置路由的模型
:param item: 模型对应的请求体item，主要用于接收前端传过来的参数
:param model_english_name: 模型英文名， 主要用于设置路径
:param model_chinese_name: 模型中文名， 主要用于设置标签和api文档
:return: None
"""
```