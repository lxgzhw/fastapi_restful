aiofiles==0.5.0
aiosqlite==0.16.1
aniso8601==7.0.0
async-exit-stack==1.0.1
async-generator==1.10
certifi==2020.12.5
chardet==4.0.0
click==7.1.2
colorama==0.4.4
dnspython==2.1.0
email-validator==1.1.2
fastapi==0.63.0
fastapi-restful==0.2.5
graphene==2.1.8
graphql-core==2.3.2
graphql-relay==2.0.1
greenlet==1.0.0
h11==0.12.0
idna==3.1
importlib-metadata==3.7.3
iso8601==0.1.14
itsdangerous==1.1.0
Jinja2==2.11.3
MarkupSafe==1.1.1
orjson==3.5.1
peewee==3.14.3
promise==2.3
pydantic==1.8.1
PyPika==0.44.1
python-dotenv==0.15.0
python-multipart==0.0.5
pytz==2020.5
PyYAML==5.4.1
requests==2.25.1
Rx==1.6.1
six==1.15.0
SQLAlchemy==1.3.23
starlette==0.13.6
tortoise-orm==0.16.21
typing-extensions==3.7.4.3
ujson==3.2.0
urllib3==1.26.3
uvicorn==0.13.4
watchgod==0.7
websockets==8.1
zipp==3.4.1
